#+title: Django 4.0 Learning
#+author: Shafiqur Rahman
#+options: h:1 num:nil toc:nil

* URL Configuration
  - Doc: https://docs.djangoproject.com/en/4.0/topics/http/urls/
** Function views
   1. Add an import:  
      #+BEGIN_SRC python
	from my_app import views
      #+END_SRC
   2. Add a URL to ~urlpatterns~: 
      #+BEGIN_SRC python
	 path('', views.home, name='home')
      #+END_SRC
** Class-based views
   1. Add an import:  
      #+BEGIN_SRC python
	from other_app.views import Home
      #+END_SRC
   2. Add a URL to ~urlpatterns~:  
      #+BEGIN_SRC python
	path('', Home.as_view(), name='home')
      #+END_SRC
** Including another URL configurations
   1. Import the include() function: 
      #+BEGIN_SRC python
	from django.urls import include, path
      #+END_SRC
   2. Add a URL to ~urlpatterns~:  
      #+BEGIN_SRC python
	path('blog/', include('blog.urls'))
      #+END_SRC
* Add Image
** Add Pillow Package from pip
   #+BEGIN_SRC bash
     pip install Pillow
   #+END_SRC
** Create a Media root and URL configuration in settings file
   #+BEGIN_SRC python
     MEDIA_ROOT = BASE_DIR / 'media'
     MEDIA_URL = '/media/'
   #+END_SRC
** Add an Image field in Model
   #+BEGIN_SRC python
     image = models.ImageField(blank=True, upload_to='images')
   #+END_SRC
** View it into HTML
   #+BEGIN_SRC html
     <img alt="{{ product.name }}" src="{{ product.image.url }}"/>
   #+END_SRC
