from django.db import models


class Products(models.Model):
    def __str__(self):
        return self.name

    name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=9, decimal_places=3)
    desc = models.TextField()
    image = models.ImageField(blank=True, upload_to='images')
