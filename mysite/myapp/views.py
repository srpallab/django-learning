from django.shortcuts import render
from .models import Products


def home(request):
    products = Products.objects.all()
    context = {
        "products": products
    }
    return render(request, 'myapp/index.html', context=context)
